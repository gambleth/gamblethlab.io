var DealerRights = artifacts.require("./DealerRights.sol");
var SicBo = artifacts.require("./SicBo.sol");

module.exports = function(deployer) {
  deployer.deploy(DealerRights).then(function(){
    return deployer.deploy(SicBo,DealerRights.address);
  });
};
