import 'bootstrap-material-design/dist/css/bootstrap-material-design.css'
import '../css/dice.css'
import '../css/app.css'

import { default as Web3 } from 'web3'
import { default as contract } from 'truffle-contract'
import dealer_rights_artifacts from '../../build/contracts/DealerRights.json'
import sicbo_artifacts from '../../build/contracts/SicBo.json'
 
import 'bootstrap-material-design'

window.addEventListener('load', function() {
// Checking if Web3 has been injected by the browser (Mist/MetaMask)
if (typeof web3 !== 'undefined') {
  window.web3 = new Web3(web3.currentProvider)
} else {
  window.web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io"))
}


var DealerRights = contract(dealer_rights_artifacts)
DealerRights.setProvider(web3.currentProvider)
// var drs = DealerRights.at('0x0ebc983f91c55a17149bdf38149911111864eea2')
var drs = DealerRights.at('0xdf5f29a28119714a00d7bf0e25f8af8dcb9a9d51')

var SicBo = contract(sicbo_artifacts)
SicBo.setProvider(web3.currentProvider)
// var sicbo = SicBo.at('0xe62101734d475af16b66a2dc8cde4c743de112ac')
var sicbo = SicBo.at('0x20f5fc3d9b49ec3aeb894a48f1d657c1ea6030b3')

var db;
window.BetOptions = {
  Small: {id:0,odds:1},
  Big: {id:1,odds:1},
  Even: {id:2,odds:1},
  Odd: {id:3,odds:1},
  Triple: {id:4,odds:30},
  Triple_1: {id:5,odds:180},
  Triple_2: {id:6,odds:180},
  Triple_3: {id:7,odds:180},
  Triple_4: {id:8,odds:180},
  Triple_5: {id:9,odds:180},
  Triple_6: {id:10,odds:180},
  Double_1: {id:11,odds:10},
  Double_2: {id:12,odds:10},
  Double_3: {id:13,odds:10},
  Double_4: {id:14,odds:10},
  Double_5: {id:15,odds:10},
  Double_6: {id:16,odds:10},
  Total_4: {id:17,odds:60},
  Total_5: {id:18,odds:30},
  Total_6: {id:19,odds:18},
  Total_7: {id:20,odds:12},
  Total_8: {id:21,odds:8},
  Total_9: {id:22,odds:7},
  Total_10: {id:23,odds:6},
  Total_11: {id:24,odds:6},
  Total_12: {id:25,odds:7},
  Total_13: {id:26,odds:8},
  Total_14: {id:27,odds:12},
  Total_15: {id:28,odds:18},
  Total_16: {id:29,odds:30},
  Total_17: {id:30,odds:60},
  Combo_1_2: {id:31,odds:6},
  Combo_1_3: {id:32,odds:6},
  Combo_1_4: {id:33,odds:6},
  Combo_1_5: {id:34,odds:6},
  Combo_1_6: {id:35,odds:6},
  Combo_2_3: {id:36,odds:6},
  Combo_2_4: {id:37,odds:6},
  Combo_2_5: {id:38,odds:6},
  Combo_2_6: {id:39,odds:6},
  Combo_3_4: {id:40,odds:6},
  Combo_3_5: {id:41,odds:6},
  Combo_3_6: {id:42,odds:6},
  Combo_4_5: {id:43,odds:6},
  Combo_4_6: {id:44,odds:6},
  Combo_5_6: {id:45,odds:6},
  Single_1: {id:46,odds:3},
  Single_2: {id:47,odds:3},
  Single_3: {id:48,odds:3},
  Single_4: {id:49,odds:3},
  Single_5: {id:50,odds:3},
  Single_6: {id:51,odds:3}
}

window.BetOptionName = ['Small','Big','Even','Odd','Any Triple','Triple One','Triple Two','Triple Three','Triple Four','Triple Five','Triple Six','Double One','Double Two','Double Three','Double Four','Double Five','Double Six','Total 4','Total 5','Total 6','Total 7','Total 8','Total 9','Total 10','Total 11','Total 12','Total 13','Total 14','Total 15','Total 16','Total 17','Has One&Two','Has One&Three','Has One&Four','Has One&Five','Has One&Six','Has Two&Three','Has Two&Four','Has Two&Five','Has Two&Six','Has Three&Four','Has Three&Five','Has Three&Six','Has Four&Five','Has Four&Six','Has Five&Sic','Has One','Has Two','Has Three','Has Four','Has Five','Has Six']

window.App = {
  dealerInterests: 0.0,
  allottedRights: 0.0,
  totalRounds: 0,
  settlementRate: 50,
  disinvestFee: 2,
  totalWagers: 0.0,

  myRights: 0.0,
  investPrice: 0.0,
  myInterests: 0.0,
  balance:0.0,
  startProgress:function(){
    $('#progress').removeClass('d-none')
    var i=1
    var interval = setInterval(function(){
      i++
      $('#progress div').css('width',i+'%')
      if(i>=95){
        clearInterval(interval)
      }      
    },350);
  },
  endProgress:function(){
    $('#progress div').css('width','100%')
    $('#progress').addClass('d-none')
  },
  bet: function (option,amount) {
    self = this
    if(amount > (self.dealerInterests / 2 / option.odds)){
      alert(BetOptionName[option.id]+' option in this round can only bet most '+(self.dealerInterests / 2 / option.odds)+',because dealers\' ETH is '+self.dealerInterests)
    }else if(amount<0.01){
      alert('Can not bet '+amount+' less than 0.01 wager')
    }else{
      console.log('option:' + BetOptionName[option.id] + ",amount:" + amount)
      var bet = {
        id: new Date().getTime(),
        gambler: web3.eth.defaultAccount,
        option: option.id,
        amount: Number(web3.toWei(amount, 'ether')),
      }
      self.saveBet(bet)
      self.startProgress()
      sicbo.bet(bet.id,bet.option,bet.amount,{from: web3.eth.defaultAccount, value: bet.amount}).then(function (res) {
        console.log(res)

        var settle = {
          id:res.logs[0].args.id.toNumber(),
          bettx : res.tx,          
          sequence: res.logs[1].args.sequence.toNumber(),
          profit : res.logs[2].args.profit.toNumber(),
          dice1 : res.logs[1].args.dice1.toNumber(),
          dice2 : res.logs[1].args.dice2.toNumber(),
          dice3 : res.logs[1].args.dice3.toNumber()
        }
        self.saveSettle(settle)
        self.refresh()
        self.getDealerInterests()
        self.endProgress()
        //self.getBetHistory()
      }).catch(function(err) {
        console.log(err)
        // Easily catch all errors along the whole execution.
        self.endProgress();
        self.deleteBet(bet.id)
      })
    }
  },
  invest: function (amount) {
    self = this;
    self.startProgress()
    sicbo.invest({ from: web3.eth.defaultAccount, value: web3.toWei(amount, 'ether'), gas:100000 }).then(function (res) {
      console.log(res)
      var invest = {
        dealer: res.logs[0].args.dealer,
        drs: res.logs[0].args.drsAmount.toNumber(),
        eth: 0 - res.logs[0].args.ethAmount.toNumber()
      }
      self.saveInvest(invest)
      self.getDealerInterests()
      self.getMyRights()
      self.getBalance()
      self.endProgress()
    })
  },
  approve: function(amount){
    self.startProgress()
    drs.approve(sicbo.address,web3.toWei(amount,'ether'),{from: web3.eth.defaultAccount, gas:100000}).then(function(value){
      console.warn(sicbo.address,value)
      self.endProgress()
    })
  },
  disinvest: function (amount) {
    self = this;
    self.startProgress()
    drs.allowance.call(web3.eth.defaultAccount,sicbo.address).then(function(value){
      if(new Number(web3.fromWei(value, 'ether')) < amount){
        alert('you want to exchang '+amount+' DRS ,but you only allow the contract to be deducted at most '+web3.fromWei(value, 'ether')+' DRS');
      }else{
        sicbo.disinvest(web3.toWei(amount, 'ether'),{from: web3.eth.defaultAccount,gas:100000}).then(function (res) {
          var invest = {
            dealer: res.logs[0].args.dealer,
            drs: 0 - res.logs[0].args.drsAmount.toNumber(),
            eth: res.logs[0].args.ethAmount.toNumber()
          }
          self.saveDisinvest(invest)
          self.getDealerInterests()
          self.getMyRights()
          self.endProgress()
        }).catch(function(err) {
          self.endProgress()
          alert("ERROR! " + err.message);
        })
      }
    })
  },
  getDealerInterests: function () {
    self = this
    sicbo.dealerInterests.call().then(function (value) {
      self.dealerInterests = web3.fromWei(value.toNumber(), 'ether')
      $('.dealer-interests').text(new Number(self.dealerInterests).toFixed(8))
      console.log('dealerInterests:' + self.dealerInterests)
    })
  },
  getTotalRounds: function () {
    self = this
    sicbo.totalRounds.call().then(function (value) {
      self.totalRounds = value.toNumber()
      $('.total-rounds').text(self.totalRounds)
      console.log('totalRounds:' + self.totalRounds)
    })
  },
  getTotalWagers: function () {
    self = this
    sicbo.totalWagers.call().then(function (value) {
      self.totalWagers = web3.fromWei(value.toNumber(),'ether')
      $('.total-wagers').text(new Number(self.totalWagers).toFixed(8))
      console.log('totalWagers:' + self.totalWagers)
    })
  },
  getMyRights:function(){
    self = this
    drs.balanceOf.call(web3.eth.defaultAccount).then(function (value) {
      self.myRights = web3.fromWei(value.toNumber(), 'ether')
      $('.my-rights').text(new Number(self.myRights).toFixed(4))
      console.log('myRights:' + self.myRights)
      self.getRightsPrice()
    })
  },
  getRightsPrice:function(){
    self = this
    sicbo.dealerInterests.call().then(function (value) {
      self.dealerInterests = web3.fromWei(value.toNumber(), 'ether')
      sicbo.allottedRights.call().then(function(allottedRights){
        self.allottedRights = allottedRights.toNumber()
        self.investPrice = web3.toWei(self.dealerInterests, 'ether') / self.allottedRights
        $('.invest-price').text(new Number(self.investPrice).toFixed(8))
        console.log('investPrice:' + self.investPrice)
        $('.my-interests').text(new Number(self.investPrice * self.myRights).toFixed(8))
        console.log('myInterests:'+self.investPrice * self.myRights)
      })
    })
  },
  getBalance:function(){
    self = this
    web3.eth.getBalance(web3.eth.defaultAccount,function (err,res) {
      self.balance = web3.fromWei(res.toNumber(), 'ether')
      $('.balance').text(new Number(self.balance).toFixed(8))
      console.log('balance:' + self.balance)
      self.getRightsPrice()
    })
  },
  getBetHistory:function(){
    self = this
    $('#my-bets').empty();
    var t = db.transaction(["bets"], "readonly");
    var store = t.objectStore("bets");
    store.openCursor().onsuccess = function(e) {
      var cursor = e.target.result;
      if(cursor) {
        self.renderBetList(cursor.value)
        cursor.continue();
      }
    }
  },
  renderBetList:function(bet){
    if(bet.profit>0){
      $('#my-bets').prepend('<tr id="bet-'+bet.id+'"><td class="tx"><a href="https://etherscan.io/tx/'+bet.tx+'" target="_blank">'+bet.id+'</a></td><td>'+BetOptionName[bet.option]+'</td><td>'+web3.fromWei(bet.amount,'ether')+'</td><td class="sequence">'+bet.sequence+'</td><td class="dices"><span class="dice-'+bet.dice1+'"></span><span class="dice-'+bet.dice2+'"></span><span class="dice-'+bet.dice3+'"></span></td><td class="profit"><span class="badge badge-success">'+new Number(web3.fromWei(bet.profit,'ether')).toFixed(6)+'</span></td></tr>') 
    }else if(bet.profit<0){
      $('#my-bets').prepend('<tr id="bet-'+bet.id+'"><td class="tx"><a href="https://etherscan.io/tx/'+bet.tx+'" target="_blank">'+bet.id+'</a></td><td>'+BetOptionName[bet.option]+'</td><td>'+web3.fromWei(bet.amount,'ether')+'</td><td class="sequence">'+bet.sequence+'</td><td class="dices"><span class="dice-'+bet.dice1+'"></span><span class="dice-'+bet.dice2+'"></span><span class="dice-'+bet.dice3+'"></span></td><td class="profit"><span class="badge badge-secondary">'+new Number(web3.fromWei(bet.profit,'ether')).toFixed(6)+'</span></td></tr>') 
    }else{
      $('#my-bets').prepend('<tr id="bet-'+bet.id+'"><td class="tx"><a href="https://etherscan.io/tx/'+bet.tx+'" target="_blank">'+bet.id+'</a></td><td>'+BetOptionName[bet.option]+'</td><td>'+web3.fromWei(bet.amount,'ether')+'</td><td class="sequence">N/A</td><td class="dices">N/A</td><td class="profit">N/A</td></tr>')
    }
  },
  updateBetList:function(bet){
    $('#bet-'+bet.id+' td.tx').html('<a href="https://etherscan.io/tx/'+bet.tx+'" target="_blank">'+bet.id+'</a>')
    $('#bet-'+bet.id+' td.sequence').html(bet.sequence)
    $('#bet-'+bet.id+' td.dices').html('<span class="dice-'+bet.dice1+'"></span><span class="dice-'+bet.dice2+'"></span><span class="dice-'+bet.dice3+'"></span>')
    if(bet.profit>0){
      $('#bet-'+bet.id+' td.profit').html('<span class="badge badge-success">'+new Number(web3.fromWei(bet.profit,'ether')).toFixed(6)+'</span>')
    }else if(bet.profit<0){
      $('#bet-'+bet.id+' td.profit').html('<span class="badge badge-secondary">'+new Number(web3.fromWei(bet.profit,'ether')).toFixed(6)+'</span>')
    }
  },
  getInvestHistory:function(){
    self = this
    $('#my-invests').empty();
    var t = db.transaction(["invests"], "readonly");
    var store = t.objectStore("invests");
    store.openCursor().onsuccess = function(e) {
      var cursor = e.target.result;
      if(cursor) {
        self.renderInvestList(cursor.value)
        cursor.continue();
      }
    }
  },
  renderInvestList:function(invest){
    $('#my-invests').prepend('<tr><td>'+invest.dealer+'</td><td>'+web3.fromWei(invest.drs,'ether')+'</td><td>'+web3.fromWei(invest.eth,'ether')+'</td></tr>')
  },
  saveBet:function(bet){
    self = this
    var t = db.transaction(["bets"],"readwrite");
    var store = t.objectStore("bets");
    console.log(bet);
    var request = store.add(bet)
    request.onsuccess = function(e){
      $('#my-bets').prepend('<tr id="bet-'+bet.id+'"><td class="tx">'+bet.id+'</td><td>'+BetOptionName[bet.option]+'</td><td>'+web3.fromWei(bet.amount,'ether')+'</td><td class="sequence">N/A</td><td class="dices">N/A</td><td class="profit">N/A</td></tr>')
    }
    request.onerror = function(e){
      console.error(e)
    }
  },
  deleteBet:function(betid){
    self = this
    var t = db.transaction(["bets"],"readwrite");
    var store = t.objectStore("bets");
    var request = store.delete(betid);
    request.onsuccess = function(e){
      $('#bet-'+betid).remove()      
    }
    request.onerror = function(e){
      console.error(e)
    }
  },
  saveSettle:function(settle){
    self = this
    setTimeout(function(){
      var tx = db.transaction(["bets"], "readwrite");
      var store = tx.objectStore("bets");
      var request = store.get(settle.id);
      self.getRightsPrice()

      request.onsuccess = function (e) {

        var bet = e.target.result
        console.log(bet)
        bet.tx = settle.bettx
        bet.sequence = settle.sequence
        bet.profit = settle.profit
        bet.dice1 = settle.dice1
        bet.dice2 = settle.dice2
        bet.dice3 = settle.dice3
        store.put(bet)
        self.updateBetList(bet)
      }
    }, 500)
  },
  saveOpen:function(open){
    var tx = db.transaction(["opens"], "readwrite");
    var store = tx.objectStore("opens");
    var request = store.add(open);
    request.onsuccess = function(e){
      console.log(open);
    }
  },
  saveInvest:function(invest){
    var t = db.transaction(["invests"],"readwrite");
    var store = t.objectStore("invests");
    
    console.log(invest);
    var request = store.add(invest)
    request.onsuccess = function(e){
      self.renderInvestList(invest)
    }
    request.onerror = function(e){
      console.error(e)
    }
  },
  saveDisinvest:function(invest){
    var t = db.transaction(["invests"],"readwrite");
    var store = t.objectStore("invests");
    
    console.log(invest);
    var request = store.add(invest)
    request.onsuccess = function(e){
      self.renderInvestList(invest)
    }
    request.onerror = function(e){
      console.error(e)
    }
  },
  setDefaultAccount: function(callback){
    self = this;
    web3.eth.getAccounts(function (err, accs) {
      if(err){
        console.log(err)
        callback(err,null)
      }else{
        if(accs[0]) {
          console.log('defaultAccount:' + accs[0])
          web3.eth.defaultAccount = accs[0]
          callback(err,web3.eth.defaultAccount)
          self.refresh()
          self.getBetHistory()
          self.getInvestHistory()
        }
        else{
          self.getDealerInterests()
          self.getTotalRounds()
          self.getTotalWagers()
          $('#message').removeClass('alert-warning').removeClass('d-none').addClass('alert-danger').alert();
          $('#message div').html('No available accounts were detected, you can not bet. Please use <a href="https://metamask.io/" target="_blank">MetaMask</a> or open this page in <a href="https://github.com/ethereum/mist/releases" target="_blank">Mist</a> or use some other DApp Browser.')
        }
      }
    })
  },
  start: function () {
    var self = this;
    const filter = web3.eth.filter('latest');
    filter.watch(function(err, res){
      if (err) {
        console.warn(err);
      } else {
        self.getBalance();
        self.getDealerInterests();
        self.getTotalRounds();
        self.getTotalWagers();
        self.getRightsPrice();
      }
    });
    
    //event Bet
    // sicbo.Bet({sender:web3.eth.defaultAccount}).watch(function (error, response) { 
      // console.warn(response.args)
    //   // if(response.args.gambler == web3.eth.defaultAccount){
    //     var bet = {
    //       sequence: response.args.sequence.toNumber(),
    //       option: response.args.option.toNumber(),
    //       amount: response.args.amount.toNumber(),
    //       time: response.args.time.toNumber()
    //     }
    //     self.saveBet(bet)
    //   // }
    // })


    //event Open
    // sicbo.Open().watch(function (error, response) { 
      // console.warn(response.args)
      // var open = {
      //   sequence : response.args.sequence.toNumber(),
      //   dice1 : response.args.dice1.toNumber(),
      //   dice2 : response.args.dice2.toNumber(),
      //   dice3 : response.args.dice3.toNumber(),
      //   blocktime:response.args.blocktime.toNumber(),
      // }
      // self.saveOpen(open)
    // })

    //event Settle
    // sicbo.Settle({sender:web3.eth.defaultAccount}).watch(function (error, response) { 
    //   console.warn(response.args)
    //   var settle = {
    //     id:response.args.betid.toNumber(),
    //     sequence: response.args.sequence.toNumber(),
    //     profit : response.args.profit.toNumber(),
    //     dice1 :response.args.dice1.toNumber(),
    //     dice2 : response.args.dice2.toNumber(),
    //     dice3 : response.args.dice3.toNumber()
    //   }
    //   self.saveSettle(settle)
    // })

    //event invest
    // sicbo.Invest({sender:web3.eth.defaultAccount}).watch(function (error, response) { 
      // console.warn(response.args)
      // // if(response.args.dealer == web3.eth.defaultAccount){
      //   var invest = {
      //     dealer: response.args.dealer,
      //     drs: response.args.drsAmount.toNumber(),
      //     eth: 0 - response.args.ethAmount.toNumber()
      //   }
      //   self.saveInvest(invest)
      // // }
    // })

    //event disinvest
    // sicbo.Disinvest({sender:web3.eth.defaultAccount}).watch(function (error, response) { 
      // console.warn(response.args)
      // // if(response.args.dealer == web3.eth.defaultAccount){
      //   var invest = {
      //     dealer: response.args.dealer,
      //     drs: 0 - response.args.drsAmount.toNumber(),
      //     eth: response.args.ethAmount.toNumber()
      //   }
      //   self.saveDisinvest(invest)
      // // }
    // })
    
    console.log('App Start');
    self.setDefaultAccount(function(error,account){
      if(!web3.isAddress(web3.eth.defaultAccount)) {
        var i = 1;
        var interval = setInterval(function () {
          console.log(i++)
          self.setDefaultAccount(function(error,account){
            if(!web3.isAddress(web3.eth.defaultAccount)) {
              clearInterval(interval)    
            }      
          })
        }, 200);
      }
    });
  },
  refresh: function () {
    var self = this;

    self.getDealerInterests()
    self.getTotalRounds()
    self.getTotalWagers()
    self.getMyRights()
    self.getBalance()
  }
}

window.betModal = function(option){  
  $('#modal-bet').modal('show')
  $('#input-bet-option').val(JSON.stringify(option))
  $('#label-bet-option').text(BetOptionName[option.id])
}

// $(window).load(function() {


  var openRequest = indexedDB.open("sicbo");
  openRequest.onupgradeneeded = function(e) {
      console.warn("indexedDB Upgrading...");
      db = e.target.result;
      var betsStore = db.createObjectStore("bets",{keyPath:'id'});
      var sequenceIndex = betsStore.createIndex('by_sequence','sequence');
      var investsStore = db.createObjectStore("invests",{autoIncrement: true});
      var opensStore = db.createObjectStore('opens',{keyPath:'sequence'});
  }
  openRequest.onsuccess = function(e) {
      console.log("indexedDB Success!");
      db = e.target.result;
      App.start()
  }
  openRequest.onerror = function(e) {
      console.error("indexedDB Error");
      console.dir(e);
  }

  $('#sicbo-address').text(sicbo.address)
  $('#drs-address').text(drs.address)
  $('#link-sicbo-address').attr('href','https://etherscan.io/address/'+sicbo.address)
  $('#link-drs-address').attr('href','https://etherscan.io/token/'+drs.address)

  $('#button-bet').click(function(){
    if (!web3.isAddress(web3.eth.defaultAccount)) {
      $('#modal-alert').modal('show')
    }else{
      $('#modal-bet').modal('hide')
      App.bet(JSON.parse($('#input-bet-option').val()),$('#input-bet-amount').val())
    }
  })

  $('#button-approve').click(function(){
    if (!web3.isAddress(web3.eth.defaultAccount)) {
      $('#modal-alert').modal('show')
    }else{
      $('#modal-approve').modal('hide')
      App.approve($('#input-approve-amount').val())
    }
  })

  $('#button-invest').click(function(){
    if (!web3.isAddress(web3.eth.defaultAccount)) {
      $('#modal-alert').modal('show')
    }else{
      $('#modal-invest').modal('hide')
      App.invest($('#input-invest-amount').val())
    }
  })

  $('#input-invest-amount').keyup(function(){
    $('#input-invest-hint').text($('#input-invest-amount').val() / App.investPrice)
  })

  $('#button-disinvest').click(function(){
    if (!web3.isAddress(web3.eth.defaultAccount)) {
      $('#modal-alert').modal('show')
    }else{
      $('#modal-disinvest').modal('hide')
      App.disinvest($('#input-disinvest-amount').val())
    }
  })

  $('#input-disinvest-amount').keyup(function(){
    $('#input-disinvest-hint').text($('#input-disinvest-amount').val() * App.investPrice)
  })
})