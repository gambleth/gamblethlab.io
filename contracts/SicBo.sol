pragma solidity ^0.4.15;

import {DealerRights} from "./DealerRights.sol";

contract SicBo {
    string public name;
    DealerRights drs;
    uint public dealerInterests;
    uint public allottedRights;
    uint32 public totalRounds;
    address owner;
    uint8 public roundGamblers;
    uint8 public settlementRate;
    uint8 public disinvestFee;
    uint public totalWagers;
    mapping (uint32 => mapping (uint8 => uint8)) public roundDices;
        
    struct Round {
        uint32 sequence;
        uint8[3] dices;
        Betting[] bets;
    }
        
    enum BetOptions{
        Small,
        Big,
        Even,
        Odd,
        Triple,
        Triple_1,
        Triple_2,
        Triple_3,
        Triple_4,
        Triple_5,
        Triple_6,
        Double_1,
        Double_2,
        Double_3,
        Double_4,
        Double_5,
        Double_6,
        Total_4,
        Total_5,
        Total_6,
        Total_7,
        Total_8,
        Total_9,
        Total_10,
        Total_11,
        Total_12,
        Total_13,
        Total_14,
        Total_15,
        Total_16,
        Total_17,
        Combo_1_2,
        Combo_1_3,
        Combo_1_4,
        Combo_1_5,
        Combo_1_6,
        Combo_2_3,
        Combo_2_4,
        Combo_2_5,
        Combo_2_6,
        Combo_3_4,
        Combo_3_5,
        Combo_3_6,
        Combo_4_5,
        Combo_4_6,
        Combo_5_6,
        Single_1,
        Single_2,
        Single_3,
        Single_4,
        Single_5,
        Single_6
    }
    
    struct Betting {
        uint id;
        uint amount;
        address gambler;
        BetOptions option;
    }
    
    Round round;
    
    event Invest(address dealer,uint ethAmount,uint drsAmount);
    event Disinvest(address dealer,uint drsAmount,uint ethAmount);
    event Bet(uint id,address gambler,uint32 sequence,BetOptions option,uint amount);
    event Open(uint32 sequence,uint8 dice1,uint8 dice2,uint8 dice3);
    event Settle(uint betid,address gambler,uint sequence,BetOptions option,int profit,uint8 dice1,uint8 dice2,uint8 dice3);
    
    function SicBo(address drsAddress) public {
        name = "Sic Bo";
        owner = msg.sender;
        drs = DealerRights(drsAddress);
        dealerInterests = 1000 wei;
        allottedRights = 100000 wei;
        totalRounds = 0;
        roundGamblers = 1; //open round by self
        settlementRate = 50; //settlement rate up limit to 50% of delaers' interests
        disinvestFee = 5; //disinvest fee is 0.5%
        totalWagers = 0;
        round.sequence = 1;
    }
    
    function () payable public {
        invest();
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    function approve(address spender,uint value) onlyOwner public returns(bool) {
        return drs.approve(spender,value);
    }

    function setRoundGamblers(uint8 value) onlyOwner public returns(uint8) {
        return roundGamblers = value;
    }

    function setSettlementRate(uint8 value) onlyOwner public returns(uint8) {
        return settlementRate = value;
    }

    function setDisinvestFee(uint8 value) onlyOwner public returns(uint8) {
        return disinvestFee = value;
    }
    
    function invest() payable public returns(uint amount) {
        require(msg.value>0);
        amount = msg.value * allottedRights / dealerInterests;
        drs.transfer(msg.sender,amount);
        allottedRights += amount;
        dealerInterests += msg.value;
        Invest(msg.sender,msg.value,amount);
        return amount;
    }
    
    function disinvest(uint amount) public returns (uint revenue) {
        require(drs.balanceOf(msg.sender) >= amount);
        var eth = amount * dealerInterests / allottedRights;//(drs.totalSupply() - drs.balanceOf(this));
        revenue = eth * (1000-disinvestFee) / 1000;
        drs.transferFrom(msg.sender,this,amount);
        msg.sender.transfer(revenue);
        dealerInterests -= eth;
        allottedRights -= amount;
        Disinvest(msg.sender,amount,revenue);
        return revenue;
    }
    
    function bet(uint id,BetOptions option) payable public returns (uint sequence) {
        require(msg.value > 1 szabo);
        totalWagers += msg.value;
        Betting memory betting;
        betting.id = id;
        betting.option = option;
        betting.gambler = msg.sender;
        betting.amount = msg.value;
        round.bets.push(betting);
        Bet(betting.id,msg.sender,round.sequence,betting.option,betting.amount);
        if (round.bets.length >= roundGamblers) {
            _roll();
        }
        return round.sequence;
    }
    
    function _roll() internal {
        var i = round.bets.length - 1;
        uint8 d1 = uint8(uint(keccak256(round.bets[i].gambler,round.bets[i].id,dealerInterests,block.number))%6) + 1;
        uint8 d2 = uint8(uint(keccak256(round.bets[i].gambler,round.bets[i].id,totalRounds,msg.gas))%6) + 1;
        uint8 d3 = uint8(uint(keccak256(round.bets[i].gambler,round.bets[i].id,totalWagers,allottedRights))%6) + 1;
        round.dices = [d1,d2,d3];
        roundDices[round.sequence][1] = d1;
        roundDices[round.sequence][2] = d2;
        roundDices[round.sequence][3] = d3;
        Open(round.sequence,d1,d2,d3);   

        _open();     
    }
    
    
    function _open() internal {
        for (uint8 i = 0; i < round.bets.length; i++) {
            if ((BetOptions.Big == round.bets[i].option && _isBig()) || (BetOptions.Small == round.bets[i].option && _isSmall()) || (BetOptions.Even == round.bets[i].option && _isEven()) || (BetOptions.Odd == round.bets[i].option && _isOdd())) {
                _win(round.bets[i],1);
            } else if (BetOptions.Triple == round.bets[i].option && _isTriple()) {
                _win(round.bets[i],30);
            } else if ((BetOptions.Triple_1 == round.bets[i].option && _isTriple(1)) || (BetOptions.Triple_2 == round.bets[i].option && _isTriple(2)) || (BetOptions.Triple_3 == round.bets[i].option && _isTriple(3)) || (BetOptions.Triple_4 == round.bets[i].option && _isTriple(4)) || (BetOptions.Triple_5 == round.bets[i].option && _isTriple(5)) || (BetOptions.Triple_6 == round.bets[i].option && _isTriple(6))) {
                _win(round.bets[i],180);
            } else if ((BetOptions.Double_1 == round.bets[i].option && _isDouble(1)) || (BetOptions.Double_2 == round.bets[i].option && _isDouble(2)) || (BetOptions.Double_3 == round.bets[i].option && _isDouble(3)) || (BetOptions.Double_4 == round.bets[i].option && _isDouble(4)) || (BetOptions.Double_5 == round.bets[i].option && _isDouble(5)) || (BetOptions.Double_6 == round.bets[i].option && _isDouble(6))) {
                _win(round.bets[i],10);
            } else if ((BetOptions.Total_4 == round.bets[i].option && _isTotal(4)) || (BetOptions.Total_17 == round.bets[i].option && _isTotal(17))) {
                _win(round.bets[i],60);
            } else if ((BetOptions.Total_5 == round.bets[i].option && _isTotal(5)) || (BetOptions.Total_16 == round.bets[i].option && _isTotal(16))) {
                _win(round.bets[i],30);
            } else if ((BetOptions.Total_6 == round.bets[i].option && _isTotal(6)) || (BetOptions.Total_15 == round.bets[i].option && _isTotal(15))) {
                _win(round.bets[i],18);
            } else if ((BetOptions.Total_7 == round.bets[i].option && _isTotal(7)) || (BetOptions.Total_14 == round.bets[i].option && _isTotal(14))) {
                _win(round.bets[i],12);
            } else if ((BetOptions.Total_8 == round.bets[i].option && _isTotal(8)) || (BetOptions.Total_13 == round.bets[i].option && _isTotal(13))) {
                _win(round.bets[i],8);
            } else if ((BetOptions.Total_9 == round.bets[i].option && _isTotal(9)) || (BetOptions.Total_12 == round.bets[i].option && _isTotal(12))) {
                _win(round.bets[i],7);
            } else if ((BetOptions.Total_10 == round.bets[i].option && _isTotal(10)) || (BetOptions.Total_11 == round.bets[i].option && _isTotal(11))) {
                _win(round.bets[i],6);
            } else if ((BetOptions.Combo_1_2 == round.bets[i].option && _isCombo(1,2)) || (BetOptions.Combo_1_3 == round.bets[i].option && _isCombo(1,3)) || (BetOptions.Combo_1_4 == round.bets[i].option && _isCombo(1,4)) || (BetOptions.Combo_1_5 == round.bets[i].option && _isCombo(1,5)) || (BetOptions.Combo_1_6 == round.bets[i].option && _isCombo(1,6)) || (BetOptions.Combo_2_3 == round.bets[i].option && _isCombo(2,3)) || (BetOptions.Combo_2_4 == round.bets[i].option && _isCombo(2,4)) || (BetOptions.Combo_2_5 == round.bets[i].option && _isCombo(2,5)) || (BetOptions.Combo_2_6 == round.bets[i].option && _isCombo(2,6)) || (BetOptions.Combo_3_4 == round.bets[i].option && _isCombo(3,4)) || (BetOptions.Combo_3_5 == round.bets[i].option && _isCombo(3,5)) || (BetOptions.Combo_3_6 == round.bets[i].option && _isCombo(3,6)) || (BetOptions.Combo_4_5 == round.bets[i].option && _isCombo(4,5)) || (BetOptions.Combo_4_6 == round.bets[i].option && _isCombo(4,6)) || (BetOptions.Combo_5_6 == round.bets[i].option && _isCombo(5,6))) {
                _win(round.bets[i],6);
            } else if (BetOptions.Single_1 == round.bets[i].option && _isSingle(1)) {
                _win(round.bets[i],_singleTimes(1));
            } else if (BetOptions.Single_2 == round.bets[i].option && _isSingle(2)) {
                _win(round.bets[i],_singleTimes(2));
            } else if (BetOptions.Single_3 == round.bets[i].option && _isSingle(3)) {
                _win(round.bets[i],_singleTimes(3));
            } else if (BetOptions.Single_4 == round.bets[i].option && _isSingle(4)) {
                _win(round.bets[i],_singleTimes(4));
            } else if (BetOptions.Single_5 == round.bets[i].option && _isSingle(5)) {
                _win(round.bets[i],_singleTimes(5));
            } else if (BetOptions.Single_6 == round.bets[i].option && _isSingle(6)) {
                _win(round.bets[i],_singleTimes(6));
            } else {
                dealerInterests += round.bets[i].amount;
                Settle(round.bets[i].id,round.bets[i].gambler,round.sequence,round.bets[i].option,0-int(round.bets[i].amount),round.dices[0],round.dices[1],round.dices[2]);
            }
        }
        
        totalRounds += 1;
        delete round;
        round.sequence = totalRounds + 1;
    }

    function _isBig() internal returns (bool) {
        var total = round.dices[0]+round.dices[1]+round.dices[2];
        return (total >= 11 && !_isTriple());
    }

    function _isSmall() internal returns (bool) {
        var total = round.dices[0]+round.dices[1]+round.dices[2];
        return (total <= 10 && !_isTriple());
    }

    function _isOdd() internal returns (bool) {
        var total = round.dices[0]+round.dices[1]+round.dices[2];
        return (total%2==1 && !_isTriple());
    }

    function _isEven() internal returns (bool) {
        var total = round.dices[0]+round.dices[1]+round.dices[2];
        return (total%2==0 && !_isTriple());
    }

    function _isTriple() internal returns (bool) {
        return (round.dices[0] == round.dices[1]) && (round.dices[1] == round.dices[2]);
    }

    function _isTriple(uint8 number) internal returns (bool) {
        return (_isTriple() && round.dices[0] == number);
    }

    function _isDouble(uint8 number) internal returns (bool) {
        return ((number == round.dices[0] && number == round.dices[1]) || (number == round.dices[0] && number == round.dices[2]) || (number == round.dices[1] && number == round.dices[2]));
    }

    function _isTotal(uint8 number) internal returns (bool) {
        return (round.dices[0]+round.dices[1]+round.dices[2] == number);
    }

    function _isCombo(uint8 number1,uint8 number2) internal returns (bool) {
        return ((round.dices[0]==number1 || round.dices[1]==number1 || round.dices[2]==number1) && (round.dices[0]==number2 || round.dices[1]==number2 || round.dices[2]==number2));
    }

    function _isSingle(uint number) internal returns (bool) {
        return (round.dices[0]==number || round.dices[1]==number || round.dices[2]==number);
    }

    function _singleTimes(uint8 number) internal returns (uint8) {
        if (_isTriple(number)) {
            return 3;
        } else if (_isDouble(number)) {
            return 2;
        } else {
            return 1;
        }
    }

    function _win(Betting betting,uint8 odds) internal {
        var wins = (betting.amount * odds) < (dealerInterests * settlementRate / 100) ? (betting.amount * odds) : (dealerInterests * settlementRate / 100);
        msg.sender.transfer((wins + betting.amount));
        dealerInterests -= wins;
        Settle(betting.id,betting.gambler,round.sequence,betting.option,int(wins),round.dices[0],round.dices[1],round.dices[2]);
    }
}